movies = ["me","myself","irene",1983]
print(movies[2])
print(movies[3])

print("      ")
print("Printing all movies using for loop")

for movie in movies:
    print(movie)

print("      ")
print("Printing all movies using while loop")
count = 0
while count < len(movies):
    print(movies[count])
    count = count + 1

print("      ")
print("Printing compound list")
facts = ["no","one", ["is", "dead"]]
print(facts[2][1])